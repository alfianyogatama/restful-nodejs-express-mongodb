const { verifyToken } = require('./../helpers/jwt')
const User = require('./../models/User')
const Product = require('./../models/Product')

const authorization = (req, res, next) => {
  try {
    const { access_token } = req.headers
    if (access_token) {
      const verify = verifyToken(access_token)
      req.user = {
        _id: verify._id,
      }
      next()
    }else{
      throw({name:"no token"})
    }
    
  } catch (error) {
    next(error)
  }
}

const authentication = async (req, res, next) => {
  try {
    const productId = req.params.product_id
    const product = await Product.findById(productId)
    if (!product) {
      throw({name:"product not found"})
    }else{
      if (product.author._id != req.user._id) {
        throw({name:"forbidden"})
      }else{
        next()
      }
    }
  } catch (error) {
    next(error)    
  }
  
}

module.exports = {
  authorization,
  authentication
}