const errorHandler = (error, req, res, next) => {
    let code = 500
    let message = 'Internal Server Error'
    // res.send(error)
  if (error.name) {
    switch (error.name){
      case "unauthorized":
        code = 401
        message = "Wrong email/password"
        break
      
      case "MongoServerError":
        code = 400
        message = 'Email already exist'
        break
      
      case "ValidationError":
        code = 400
        let errMessage = []
        for(let key in error.errors){
          errMessage.push(error.errors[key].message)
        }
        message = errMessage
        break
      
      case "no token":
        code = 403
        message = ['Please login first']
        break
      
      case "product not found":
        code = 404
        message = ["Product not found"]
        break
      
      case "CastError":
        code = 400
        message = ["Invalid id"]
        break

      case "forbidden":
        code = 403
        message = ["You don`t have permission to edit/delete this file"]
        break
      
      case "edit product failed":
        code = 400
        message = error.name
        
        }
        res.status(code).json({message})
  }else{
    res.status(code).json({error})
  }
}

module.exports = errorHandler