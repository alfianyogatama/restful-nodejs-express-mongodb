# My Assets App Server
My Assets App is an application to manage your assets. This app has : 
* RESTful endpoint for asset's CRUD operation
* JSON formatted response

&nbsp;

## RESTful endpoints
### POST /register

> Register User

_Request Header_
```
not needed

```

_Request Body_

```json
{
    "username": "<String>",
    "email": "<String>",
    "password": "<String>",
    "gender": "<String>",
    "birth_date": "<yyyy-mm-dd>",
    "address": {
        "provinsi": "<String>",
        "kabupaten": "<String>",
        "kecamatan": "<String>",
        "desa": "<String>"
    }
}
```

_Response (201 - created)_

```json
{
    "user": {
        "_id": "<Generated Object Id>",
        "username": "<String>",
        "email": "<String>",
        "birth_date": "17/9/1994 07.00.00",
        "address": [
            {
                "provinsi": "<String>",
                "kabupaten": "<String>",
                "kecamatan": "<String>",
                "desa": "<String>",
                "_id": "<Generated Object Id>"
            }
        ]
    }
}

```

_Response (400 - Bad Request)_

```json
{
    "message": {
        "email": {
            "name": "ValidatorError",
            "message": "Path <field> is required.",
            "properties": {
                "message": "Path <field> is required.",
                "type": "required",
                "path": "<field>"
            },
            "kind": "required",
            "path": "<field>"
        }
    }
}

```