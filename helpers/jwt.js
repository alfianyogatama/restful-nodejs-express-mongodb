const jwt = require('jsonwebtoken')

const createToken = (obj) => {
  return jwt.sign(obj, process.env.JWT_TOKEN)
}

const verifyToken = (token) => {
  return jwt.verify(token, process.env.JWT_TOKEN)
}

module.exports = {
  createToken,
  verifyToken
}