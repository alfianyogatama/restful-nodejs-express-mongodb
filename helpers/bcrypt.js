const bcrypt = require('bcryptjs')
const salt = 8

const hashPass = (password) => {
  return bcrypt.hashSync(toString(password), salt)
}

const checkPass = (password, hash) => {
  return bcrypt.compareSync(toString(password), hash)
}

module.exports = {
  hashPass,
  checkPass
}