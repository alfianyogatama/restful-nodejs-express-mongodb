if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config({ debug: process.env.DEBUG })
}
const express = require('express')
const app = express()
const port = process.env.port || 3000
const router = require('./routes/')
const mongooseConnect = require('./config/mongoose')

app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(router)

app.listen(port, () => {
  mongooseConnect()
  console.log('listening', port)
})