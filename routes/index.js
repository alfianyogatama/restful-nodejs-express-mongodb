const router = require('express').Router()
const userController = require('./../controllers/userController')
const errorHandler = require('./../middlewares/errorHandler')
const productRoutes = require('./productRoutes')

router.get('/', (req, res) => {
  res.send('hello world')
})

router.post('/register', userController.register)
router.post('/login', userController.login)

router.use('/products', productRoutes)

router.use(errorHandler)

module.exports = router