const router = require('express').Router()
const productController = require('./../controllers/productController')
const { authorization, authentication } = require('./../middlewares/authorization')

router.get('/', productController.getAllProduct)
router.get('/find', productController.findProductByCategory)

router.use(authorization)
router.post('/', productController.addproduct)
router.get('/:product_id', productController.getProduct)
router.put('/edit/:product_id', authentication, productController.editProduct)
router.delete('/delete/:product_id', authorization, productController.deleteProduct)

module.exports = router