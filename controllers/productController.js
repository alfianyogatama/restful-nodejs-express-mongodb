const Product = require('./../models/Product')

class Controller {
  static async addproduct(req, res, next) {
    try {
      const payload = {...req.body, author:{_id: req.user._id}}
      console.log(payload)
      const product = new Product({...payload}) 
      const result = await product.save()
      if(result){
        res.status(200).json({
          product: result,
          message: "New product succesfully  created"
        })
      }     
    } catch (error) {
      console.log(error)
      next(error)
    }
  }

  static async getProduct(req, res, next) {
    try {
      const result = await Product.findById(req.params.product_id)
      if (!result) {
        throw({name:"product not found"})
      }else{
        res.status(200).json(result)
      }
    } catch (error) {
        console.log(error.name)
        next(error)
    }
  }

  static async getAllProduct(req, res, next) {
    try {
      const result = await Product.find()
      res.status(200).json(result)
    }catch(error) {
      next(error)
    }
  }

  static async editProduct(req, res, next) {
    try {
      const payload = {...req.body}
      const result = await Product.updateOne({ _id: req.params.product_id}, payload)
      if (result.acknowledged) {
        const product = await Product.findById(req.params.product_id)
        res.status(200).json({product, message: "Product edited successfully"})
      }else{
        throw({message: "edit product failed"})
      }
    } catch (error) {
      next(error)
    }
  }

  static async deleteProduct(req, res, next) {
    try {
      const product = await Product.findById(req.params.product_id)
      if (!product) {
        throw({name: "product not found"})
      }else{
        const result = await Product.deleteOne({_id: product.id})
        res.status(200).json({message: 'This product successfully deleted'})
      }
    } catch (error) {
      next(error)
    }
  }

  static async findProductByCategory(req, res, next) {
    try {
      const category = {
        name: req.query.category
      }
      
      console.log(category)
      const result = await Product.find({category})
      if (result.length === 0 ) {
        throw({name: "product not found"})
      }else{
        res.status(200).json(result)
      }
      
    } catch (error) {
      next(error)
    }
  }

  static async
  
}

module.exports = Controller