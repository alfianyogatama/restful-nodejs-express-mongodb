const User = require('../models/User')
const { createToken } = require('../helpers/jwt')
const { hashPass, checkPass } = require('./../helpers/bcrypt')

class Controller {
  static async register(req, res, next) {
    try {
      let password = hashPass(req.body?.password)
      let payload = {...req.body, password}
      const user = new User(payload)
      if(user){
        const result = await user.save()
        console.log(result.validateSync())
        if(result){
          res.status(201).json({
            user: 
            {
              username: result.username,
              email: result.email,
              birth_date: result.birth_date.toLocaleString("id-id"),
              address: result.address,
            },
            message: "New user successfully created"
          })
        }
      }
    } catch (error) {
      next(error)
    }
  }

  static async login(req, res, next) {

    try {
      const {email, password} = req.body
      const query = User.where({email})
      const user = await query.findOne()
      if (!user) {
        throw({name:"unauthorized"})
      }else{
        let pass = checkPass(password, user.password)
        if (!pass) {
          throw({name:"unauthorized"})
        }else{
          const access_token = createToken({
            _id: user._id,
            username: user.username
          })

          res.status(200).json({
            access_token,
            message: "Successfully Login"
          })
        }
      }
      
    } catch (err) {
      next(err)
    }
  }
}

module.exports = Controller