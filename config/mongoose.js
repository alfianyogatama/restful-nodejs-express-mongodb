const mongoose = require('mongoose');
const mongoDB = 'mongodb://127.0.0.1/ecommerce_db';

const mongooseConnect = async () => {
  try {
    const connection = await mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })

    if (connection) {
      console.log('Conncected to DB')
    }
    
  } catch (error) {
    console.log(error)
    return error
  }

}

module.exports = mongooseConnect