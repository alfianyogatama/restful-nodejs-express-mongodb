const mongoose = require('mongoose')
const validateEmail =  require('./../helpers/emailValidation')
const userSchema = mongoose.Schema({
  username: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: [true, 'email already exist'],
    required: true,
    validate: [ validateEmail, 'Invalid email format'], 
  },
  gender: {
    type: String,
    enum: ['male', 'female'],
    required: true
  },
  password: {
    type: String,
    required: true
  },
  birth_date: {
    type: Date,
    required: true
  },
  address: [{
    provinsi: {
      type: String,
      required: true
    },
    kabupaten: {
      type: String,
      required: true
    },
    kecamatan: {
      type: String,
      required: true
    },
    desa: {
      type: String,
      required: true
    }
  }]
})

module.exports = mongoose.model('User', userSchema)