const mongoose = require('mongoose')
const productSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  }, 
  imgUrl: {
    type: String,
    required: [true, 'Image url is required']
  },
  category: {
    name: {
      type: String,
      required: [true, 'Product category is required']
    }
  },
  author: {
    _id: {
      type: mongoose.ObjectId,
      required: true
    },
  }
})

module.exports = mongoose.model('Product', productSchema)